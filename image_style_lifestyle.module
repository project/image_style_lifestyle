<?php
/**
 * @file
 * Image style lifestyle or render module.
 */

/**
 * Implements hook_image_effect_info().
 */
function image_style_lifestyle_image_effect_info() {
  $effects['image_style_lifestyle'] = array(
    'label' => t('Lifestyle or Render Switcher'),
    'help' => t('Use different effects depending on whether the image is a render or a lifestyle image.'),
    'effect callback' => 'image_style_lifestyle_effect_callback',
    'form callback' => 'image_style_lifestyle_form',
  );

  return $effects;
}

/**
 * Image style callback effect.
 *
 * @param stdClass $image
 *   An image object returned by image_load().
 * @param array $data
 *   Array of saved image style data.
 *
 * @return bool
 *   TRUE on success otherwise FALSE.
 */
function image_style_lifestyle_effect_callback(stdClass $image, array $data = array()) {

  $style_name = _image_style_lifestyle_is_render($image, $data) ? $data['render'] : $data['lifestyle'];

  // They didn't select an image style.
  if (empty($style_name)) {
    return TRUE;
  }

  $style = image_style_load($style_name);

  if (empty($style)) {
    // Required preset has gone missing?
    watchdog('image_style_lifestyle', "When running 'lifestyle or render switcher' action, I was unable to load sub-action %style_name. Either it's been deleted or the DB needs an update", array('%style_name' => $style_name), WATCHDOG_ERROR);
    return FALSE;
  }

  // Run the preset actions.
  foreach ($style['effects'] as $sub_effect) {
    image_effect_apply($image, $sub_effect);
  }
  return TRUE;
}

/**
 * Attempt to detect a render using the corner pixel or the image.
 *
 * @param stdClass $image
 *   An image object returned by image_load().
 * @param array $settings
 *   The array of settings from the image style form.
 *
 * @return bool
 *   TRUE if it's a render image, FALSE otherwise.
 */
function _image_style_lifestyle_is_render($image, $settings) {
  $callback = _image_style_lifestyle_is_render_callback();

  if (function_exists($callback)) {
    return $callback($image, $settings);
  }
  return _image_style_lifestyle_corner_is_white($image, $settings);
}

/**
 * Checks if the corner of the image is white.
 *
 * @param stdClass $image
 *   An image object returned by image_load().
 * @param array $settings
 *   The array of settings from the image style form.
 *
 * @return bool
 *   TRUE if the 0,0 pixel is #FFF.
 */
function _image_style_lifestyle_corner_is_white(stdClass $image, $settings) {
  if (_image_style_lifestyle_is_transparent($image)) {
    return TRUE;
  }

  // Get the colour of the top level pixel.
  $rgb = imagecolorat($image->resource, 0, 0);

  return _image_style_lifestyle_within_tolerance($rgb, $settings['tolerance']);
}

/**
 * Samples the image for an average colour.
 *
 * @param stdClass $image
 *   An image object returned by image_load().
 * @param array $settings
 *   The array of settings from the image style form.
 *
 * @return bool
 *   TRUE if the average colour is #FFF or transparent.
 */
function _image_style_lifestyle_average_colour_is_near_white(stdClass $image, $settings) {
  if (_image_style_lifestyle_is_transparent($image)) {
    return TRUE;
  }

  // Create a new image resource 1x1.
  $new_image = imagecreatetruecolor(1, 1);

  // Get the x and y of the image we're testing.
  $width = imagesx($image->resource);
  $height = imagesy($image->resource);

  // Resize the image into the 1x1 canvas.
  imagecopyresampled($new_image, $image->resource, 0, 0, 0, 0, 1, 1, $width, $height);

  // Get the colour of the new image.
  $rgb = imagecolorat($new_image, 0, 0);

  return _image_style_lifestyle_within_tolerance($rgb, $settings['tolerance']);
}

/**
 * Check if the colour is within the tolerance setting.
 *
 * @param int $rgb
 *   The rgb colour.
 * @param int $tolerance
 *   The tolerance as a percentage.
 *
 * @return bool
 *   TRUE if the colour is within our tolerance level.
 */
function _image_style_lifestyle_within_tolerance($rgb, $tolerance) {
  // 16777215 is white.
  $percentile = 16777215 - (16777215 * ($tolerance / 100));
  return $rgb > $percentile;
}

/**
 * Check if the image has a transparent background.
 *
 * @param stdClass $image
 *   An image object returned by image_load().
 *
 * @return bool
 *   TRUE if the background is transparent.
 */
function _image_style_lifestyle_is_transparent(stdClass $image) {
  $rgb = imagecolorat($image->resource, 0, 0);
  $a = $rgb >> 24;

  return $a == 127;
}

/**
 * Get the callback for determining if it's a render image.
 *
 * @return string
 *   The callback function.
 */
function _image_style_lifestyle_is_render_callback() {
  $callback = "_image_style_lifestyle_corner_is_white";
  drupal_alter('image_style_lifestyle_is_render', $callback);

  return $callback;
}

/**
 * Image effect form callback for the lifestyle or render effect.
 *
 * @param array $data
 *   The current configuration for this image effect.
 *
 * @return array
 *   The form definition for this effect.
 */
function image_style_lifestyle_form(array $data) {
  $defaults = array(
    'lifestyle' => '',
    'render' => '',
    'tolerance' => 30,
  );
  $data = array_merge($defaults, (array) $data);

  $form = array(
    'help' => array(
      '#markup' => t('You must create the two presets to use <em>before</em> enabling this process.'),
    ),
  );

  // The PASS_THROUGH parameter is new as of D7.23, and is added here to prevent
  // image_style_options() from double-encoding the human-readable image style
  // name, since the form API will already sanitize options in a select list.
  $styles = image_style_options(TRUE, PASS_THROUGH);
  $form['render'] = array(
    '#type' => 'select',
    '#title' => t('Style to use if the image is a render'),
    '#default_value' => $data['render'],
    '#options' => $styles,
    '#description' => t('If you choose none nothing will happen.'),
  );
  $form['lifestyle'] = array(
    '#type' => 'select',
    '#title' => t('Style to use if the image is a lifestyle image.'),
    '#default_value' => $data['lifestyle'],
    '#options' => $styles,
    '#description' => t('If you choose none nothing will happen.'),
  );
  $form['tolerance'] = array(
    '#type' => 'textfield',
    '#title' => t('The tolerance to use when detecting the image colour.'),
    '#default_value' => $data['tolerance'],
    '#description' => t('The higher the tolerance the more likely to be selected as a render. Default 30%'),
  );
  return $form;
}
